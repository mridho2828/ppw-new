from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json

# Create your views here.
def index(request):
    return render(request, 'lab_9/index.html')

def endpoint(request):
    response = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    return JsonResponse(response.json())