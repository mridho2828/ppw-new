from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .views import index, endpoint

# Create your tests here.
class Lab9UnitTest9(TestCase):
    
    def test_url_landing_page_is_exist(self):
        response = Client().get('/lab_9/')
        self.assertEqual(response.status_code, 200)

    def test_url_endpoint_is_exist(self):
        response = Client().get('/lab_9/endpoint/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/lab_9/')
        self.assertEqual(found.func, index)

    def test_using_endpoint_func(self):
        found = resolve('/lab_9/endpoint/')
        self.assertEqual(found.func, endpoint)


class Lab9FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab9FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab9FunctionalTest, self).tearDown()

