from django.shortcuts import render, redirect
from django.contrib.auth import logout as logout_user

def index(request):
    return render(request, 'lab_11/index.html')

def logout(request):
    logout_user(request)
    return redirect('/lab_11/')