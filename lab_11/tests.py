from django.test import TestCase, Client
from django.urls import resolve

# Create your tests here.
class Lab11UnitTest(TestCase):
    
    def test_url_landing_page_is_exist(self):
        response = Client().get('/lab_11/')
        self.assertEqual(response.status_code, 200)

    def test_login_page_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)