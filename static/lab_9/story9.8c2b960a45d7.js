$(document).ready(function() {
    $.get('lab_9/endpoint', function(result) {
        result.items.forEach(function(book, index) {
            $('#content').append(`
                <tr>
                    <th scope="row">${index+1}</th>
                    <td>${book.volumeInfo.title}</td>
                    <td>${book.volumeInfo.authors.join(', ')}</td>
                    <td>${book.volumeInfo.publisher}</td>
                    <td class="text-center">
                        <img class="star favourite"
                            src="/static/lab_9/star.svg"
                            onmouseover='lightStar(this)'
                            onmouseout='darkStar(this)'/>
                    </td>
                </tr>
            `)
        })
        $('.star').click(starClickedNotFav)
    })
})

function lightStar(element) {
    $(element).attr('src', '/static/lab_9/star-light.svg')
}

function darkStar(element) {
    $(element).attr('src', '/static/lab_9/star.svg')
}

function starClickedNotFav() {
    var nextCnt = parseInt($("#favourite_count").html()) + 1
    $("#favourite_count").html(nextCnt.toString())
    lightStar(this)
    $(this).removeAttr('onmouseover')
    $(this).removeAttr('onmouseout')
    $(this).off('click')
    $(this).click(starClickedFav)
}

function starClickedFav() {
    var nextCnt = parseInt($("#favourite_count").html()) - 1
    $("#favourite_count").html(nextCnt.toString())
    darkStar(this)
    $(this).attr('onmouseover', lightStar)
    $(this).attr('onmouseout', darkStar)
    $(this).off('click')
    $(this).click(starClickedNotFav)
}
