$(document).ready(function() {
    $.get('/lab_9/endpoint', function(result) {
        result.items.forEach(function(book, index) {
            $('#content').append(`
                <tr>
                    <td>${index+1}</td>
                    <td><img src="${book.volumeInfo.imageLinks.smallThumbnail}"></td>
                    <td>${book.volumeInfo.title}</td>
                    <td>${book.volumeInfo.authors}</td>
                    <td>${book.volumeInfo.publisher}</td>
                    <td class="text-center">
                        <img class="star"
                            src="/static/lab_9/star.svg"
                            onmouseover='lightStar(this)'
                            onmouseout='darkStar(this)'/>
                    </td>
                </tr>
            `)
        })
        $('.star').click(starClickedNotFav)
    })
})

function lightStar(element) {
    $(element).attr('src', '/static/lab_9/star-light.svg')
}

function darkStar(element) {
    $(element).attr('src', '/static/lab_9/star.svg')
}

function starClickedNotFav() {
    var nextCnt = parseInt($("#favorite-count").html()) + 1
    $("#favorite-count").html(nextCnt.toString())
    lightStar(this)
    $(this).removeAttr('onmouseover')
    $(this).removeAttr('onmouseout')
    $(this).off('click')
    $(this).click(starClickedFav)
}

function starClickedFav() {
    var nextCnt = parseInt($("#favorite-count").html()) - 1
    $("#favorite-count").html(nextCnt.toString())
    darkStar(this)
    $(this).attr('onmouseover', lightStar)
    $(this).attr('onmouseout', darkStar)
    $(this).off('click')
    $(this).click(starClickedNotFav)
}
