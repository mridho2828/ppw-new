$(document).ready(function() {
    $.get('lab_9/endpoint', function(result) {
        result.items.forEach(function(book, index) {
            $('#content').append(`
                <tr>
                    <th scope="row">${index+1}</th>
                    <td>${book.volumeInfo.title}</td>
                    <td>${book.volumeInfo.authors.join(', ')}</td>
                    <td>${book.volumeInfo.publisher}</td>
                    <td class="text-center">
                        <img class="star favourite"
                            src="/static/star.svg"
                            onmouseover='lightStar(this)'
                            onmouseout='darkStar(this)'/>
                    </td>
                </tr>
            `)
        })
        $('.star').click(starClickedNotFav)
    })
})
