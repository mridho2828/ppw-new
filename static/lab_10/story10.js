function validateAll() {
    if (window.emailValid && window.nameValid && window.passwordValid) {
        $('#submit-subscribe').removeClass('disabled')
    } else {
        $('#submit-subscribe').addClass('disabled')
    }
}

function validateEmail() {
    console.log("ae2wwa2ef")
    var email = $('.emailClass').val()
    $.post('lab_10/check_email/', {
        email: email
    }, function (response) {
        if (response.subscribed) {
            $('.emailClass').addClass('is-invalid')
            $('.invalid-feedback.email').remove()
            $('.emailClass').after(`<div class="invalid-feedback email">
                Email tersebut sudah pernah didaftarkan sebelumnya, silakan daftar dengan email lain.
            </div>`)
            window.emailValid = false
        } else if (!response.valid_email) {
            $('.emailClass').addClass('is-invalid')
            $('.invalid-feedback.email').remove()
            $('.emailClass').after(`<div class="invalid-feedback email">
                Masukkan alamat email yang valid.
            </div>`)
            window.emailValid = false
        } else {
            $('.emailClass').removeClass('is-invalid')
            $('.emailClass').addClass('is-valid')
            window.emailValid = true
        }
        validateAll()
    })
}

function validateName() {
    var name = $('.nameClass').val()
    if (name.length > 0) window.nameValid = true
    else window.nameValid = false
    validateAll()
}

function validatePassword() {
    var name = $('.passwordClass').val()
    if (name.length > 0) window.passwordValid = true
    else window.passwordValid = false
    validateAll()
}

$('.emailClass').keyup(validateEmail);
$('.nameClass').keyup(validateName)
$('.passwordClass').keyup(validatePassword)

$('#submit-subscribe').click(function submitSubscribe(e) {
    e.preventDefault()
    var token = $('input[name=csrfmiddlewaretoken]').val()
    var email = $('.emailClass').val()
    var name = $('.nameClass').val()
    var password = $('.passwordClass').val()
    $.post('/lab_10/', {
        csrfmiddlewaretoken: token,
        email: email,
        name: name,
        password: password,
    }, function (response) {
        $('#subscribed-modal').modal()
    })
})

$('#subscribe-form').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
      e.preventDefault();
      return false;
    }
});
