var isPrimary = true;

function changeTheme() {
    if (isPrimary) {
        $("body").css({"background-color": "black", "color": "white"});
        isPrimary = false;
    }  
    else {
        $("body").css({"background-color": "white", "color": "black"});
        isPrimary = true;
    }
}

function accordion(id) {
    $("#panel-" + id).slideToggle();
    $("#panel-" + id).removeClass("hidden");
}

function move() {
    var bar = document.getElementById("bar");
    var width = 1;
    var id = setInterval(frame, 10);
    function frame() {
        if (width >= 100) {
            clearInterval(id);
        } else {
            width++;
            bar.style.width = width + '%';
        }
    }
    $(document).ready(function(){ 
        setTimeout(function(){ 
            $("#progress").addClass("hidden");
            $("#content").removeClass("hidden");   
        },
        1100); 
    });
}