var isPrimary = true;

function changeTheme() {
    if (isPrimary) {
        $("body").removeClass("bg-primary");
        isPrimary = false;
    }  
    else {
        $("body").addClass("bg-primary");
        isPrimary = true;
    }   
}


function accordion(id) {
    $("#panel-" + id).slideToggle();
    $("#panel-" + id).removeClass("hidden");
}
