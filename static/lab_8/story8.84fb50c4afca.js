var isPrimary = true;

function changeTheme() {
    if (isPrimary) {
        $("body").addClass("bg-black");
        isPrimary = false;
    }  
    else {
        $("body").addClass("bg-primary");
        isPrime = true;
    }   
}

function accordion(id) {
    $("#body-" + id).slideToggle();
    $("#body-" + id).removeClass("hidden");
}
