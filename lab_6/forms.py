from django import forms
from .models import Status


class Status_Form(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['content']
        labels = {
            'content': '',
        }
        widgets = {
            'content': forms.Textarea(attrs={'cols':70, 'rows':10, 'id': 'content'}),
        }