from django.urls import re_path
from . import views

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^add_status/$', views.add_status, name='add_status'),
    re_path(r'^challenge/$', views.challenge, name='challenge')
]
