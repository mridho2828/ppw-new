from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from .models import Status
from .views import index, add_status, challenge
from .forms import Status_Form
from selenium import webdriver
from selenium.webdriver.support.color import Color
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.

class Lab6FunctionalTest(TestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab6FunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_post_status(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/lab_6/')

        content = selenium.find_element_by_id('content')
        submit = selenium.find_element_by_id('submit-button')

        content.send_keys('Coba Coba')
        submit.submit()
        time.sleep(5)

        selenium.get('http://127.0.0.1:8000/lab_6/')
        assert 'Coba Coba' in selenium.page_source

    # def test_layout1(self):
    #     selenium = self.selenium
    #     selenium.get('http://ppw-e-mridho2828.herokuapp.com/')
        
    #     title = selenium.title
    #     self.assertEqual('Update Status', title)

    # def test_layout2(self):
    #     selenium = self.selenium
    #     selenium.get('http://ppw-e-mridho2828.herokuapp.com/')

    #     greetings = selenium.find_element_by_tag_name('h1')
    #     self.assertEqual('Hello, apa kabar?', greetings.text)

    # def test_css1(self):
    #     selenium = self.selenium
    #     selenium.get('http://ppw-e-mridho2828.herokuapp.com/')

    #     section_title = selenium.find_element_by_css_selector('p.section-title')
    #     self.assertEqual('30px', section_title.value_of_css_property('font-size'))

    # def test_css2(self):
    #     selenium = self.selenium
    #     selenium.get('http://ppw-e-mridho2828.herokuapp.com/')

    #     background_color = selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
    #     hex_color = Color.from_string(background_color).hex
    #     self.assertEqual('#ffffff', hex_color)

    # def test_post_to_heroku(self):
    #     selenium = self.selenium
    #     selenium.get('http://ppw-e-mridho2828.herokuapp.com/')
        
    #     content = selenium.find_element_by_id('content')
    #     submit = selenium.find_element_by_id('submit-button')

    #     content.send_keys('Coba Coba')
    #     submit.submit()
    #     time.sleep(5)
    #     assert 'Coba Coba' in selenium.page_source


class Lab6UnitTest(TestCase):

    def test_url_landing_page_is_exist(self):
        response = Client().get('/lab_6/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/lab_6/')
        self.assertEqual(found.func, index)

    def test_using_add_status_func(self):
        found = resolve('/lab_6/add_status/')
        self.assertEqual(found.func, add_status)

    def test_using_index_template(self):
        response = Client().get('/lab_6/')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_contains_greeting(self):
        response = Client().get('/lab_6/')
        html_respone = response.content.decode('utf-8')
        self.assertIn('Hello, apa kabar?', html_respone)

    def test_model_can_create_new_status(self):
        Status.objects.create(date=timezone.now(), content='h4h4 h3h3')
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_successfully_create_object(self):
        form = Status_Form({'date': timezone.now(), 'content': 'h4h4 h3h3'})
        self.assertTrue(form.is_valid())
        status = form.save()
        self.assertEqual(status.content, 'h4h4 h3h3')

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'date': '', 'content': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['content'],
            ['This field is required.']
        )
        
    def test_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/lab_6/add_status/', {'date': timezone.now(), 'content': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/lab_6/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/lab_6/add_status/', {'date': '', 'content': ''})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/lab_6/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    # for challenge
    def test_url_challenge_is_exist(self):
        response = Client().get('/lab_6/challenge/')
        self.assertEqual(response.status_code, 200)

    def test_using_challenge_func(self):
        found = resolve('/lab_6/challenge/')
        self.assertEqual(found.func, challenge)

    def test_using_challenge_template(self):
        response = Client().get('/lab_6/challenge/')
        self.assertTemplateUsed(response, 'challenge.html')
