from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Status
from .forms import Status_Form

# Create your views here.
def index(request):
    response = {}
    response['status_list'] = Status.objects.all()
    response['status_form'] = Status_Form
    return render(request, 'index.html', response)

def add_status(request):
    response = {}
    form = Status_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['content'] = request.POST['content']
        status = Status(content=response['content'])
        status.save()

    return HttpResponseRedirect('/')

def challenge(request):
    return render(request, 'challenge.html')