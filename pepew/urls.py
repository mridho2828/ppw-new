"""pepew URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.contrib.auth.views import LoginView
from lab_11.views import logout

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(('lab_8.urls', 'lab_8'), namespace='lab_8')),
    path('lab_6/', include(('lab_6.urls', 'lab_6'), namespace='lab_6')),
    path('lab_9/', include(('lab_9.urls', 'lab_9'), namespace='lab_9')),
    path('lab_10/', include(('lab_10.urls', 'lab_10'), namespace='lab_10')),
    path('lab_11/', include(('lab_11.urls', 'lab_11'), namespace='lab_11')),
    path('auth/', include('social_django.urls', namespace='social')),
    path('login/', LoginView.as_view(template_name='lab_11/login.html'), name='login'),
    path('logout/', logout, name='logout'),
]
