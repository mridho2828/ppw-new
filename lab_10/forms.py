from django import forms
from .models import Subscriber


class Subscriber_Form(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = '__all__'
        widgets = {
            'email': forms.TextInput(attrs={'class': 'emailClass'}),
            'name': forms.TextInput(attrs={'class': 'nameClass'}),
            'password': forms.PasswordInput(attrs={'class': 'passwordClass'}),
        }