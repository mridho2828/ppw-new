from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import Subscriber_Form
from .models import Subscriber
from django.core.validators import validate_email

# Create your views here.
def index(request):
    form = Subscriber_Form(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=400)

    else:
        form = Subscriber_Form()
        return render(request, 'lab_10/index.html', {'form': form})

@csrf_exempt
def check_email(request):
    email = request.POST['email']
    num_user = Subscriber.objects.filter(email=email).count()
    try:
        validate_email(email)
        valid_email = True
    except:
        valid_email = False
    response = {
        'valid_email': valid_email,
        'subscribed': num_user == 1,
    }
    return JsonResponse(response)
