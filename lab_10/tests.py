from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.
class Lab10UnitTest(TestCase):
    
    def test_url_landing_page_is_exist(self):
        response = Client().get('/lab_10/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/lab_10/')
        self.assertEqual(found.func, index)
